FROM ritikaniyengar/base_yolo 
ENV PYTHONIOENCODING='utf8'

WORKDIR /yolo

# Copy contents
COPY . /yolo

# Set environment variables
ENV HOME=/yolo


RUN pip3 install redis
RUN pip3 install pysftp
RUN pip3 uninstall numpy -y
RUN pip3 install numpy
RUN pip3 install -r requirements.txt

CMD ["python3", "livis_inference/main.py"]

