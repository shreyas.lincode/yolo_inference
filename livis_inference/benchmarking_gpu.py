'''
use script to specify three fields
    -model to inference with
    -batch size to inference with
    -lastly the folder path to images to inference with
return a excel sheet with bench mark info

example command:

python benchmarking_gpu.py --model yolov5s --batch_size 8 --folderpath /home/lincode/Documents/Prajodh/YOLOV5_inferencing/yolo_inference/yolov5-master/images-1

'''
import torch
import datetime
import openpyxl
import os
from pathlib import Path
from detect import run
import nvidia_smi
import numpy as np
import statistics
import re
import argparse

Model_name="yolov5s"
Batch_size=4
file_name=os.getcwd()+"/"+"images-1" #512 images


parser=argparse.ArgumentParser()
parser.add_argument("-m","--model",help="specify yolov5 model \n example:yolov5s")
parser.add_argument("-b","--batch_size",help="specify the batch size \n example:integer vals like 4,8,16.....",type=int)
parser.add_argument("-f","--folderpath",help="specify the filepath \n example:/home/lincode/Documents/Prajodh/YOLOV5_inferencing/yolo_inference/yolov5-master/images-1")
args=parser.parse_args()

if args.model:
    Model_name=args.model
if args.batch_size:
    Batch_size=args.batch_size
if args.folderpath:
     file_name=args.folderpath

'''
Modify these three fields and experiment
'''



def get_gpu_consumption():
    nvidia_smi.nvmlInit()

    deviceCount = nvidia_smi.nvmlDeviceGetCount()
    for i in range(deviceCount):
        handle = nvidia_smi.nvmlDeviceGetHandleByIndex(i)
        info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
        nvidia_smi.nvmlShutdown()
        return info.used
      
  
def no_of_files(path):
    for dirpath,dirname,filenames in os.walk(path):
        return len(filenames)

def multiple_image_inference_benchmark(image_list,batch_size,model,gpu_before_inference,sheet,row_number):
    '''
    does batch inferencing of the video
    '''
    avg_gpu_consumed=[]
    counter_variable=1
    images=image_list
    batch_size_list=[]

    while counter_variable*batch_size < len(images):
         batch_size_list.append(counter_variable*batch_size)
         counter_variable+=1
    
    images=np.split(images,batch_size_list)
    for img in images:
        imgs=[file['dirpath']+"/"+file['filename'] for file in img]
        results=model(imgs)
        results.save()
        avg_gpu_consumed.append((get_gpu_consumption()-gpu_before_inference)/1000000000)
    print("AVAERAGE GPU CONSUMED",statistics.mean(avg_gpu_consumed))
    sheet['D'+row_number]=statistics.mean(avg_gpu_consumed)

def single_video_inference(video_file):
     '''
     does video inferencing
     '''
     path_best_pt="best.pt"
     folder_save='runs/video'
     folder_name='video'
     run(weights=path_best_pt,source=video_file,project=folder_save,name=folder_name)



def inference_main_based_on_file_type(filename,batch_size,model,gpu_before_inference,sheet,row_number):
      '''
      input : unzipped folder name
      functionality: to do the appropriate inferening based on file type(images and videos)
      output:None
      '''
      images_list=[]
      for dirpath,dirname,filenames in os.walk(filename):
            for file in filenames:  
                match=re.compile(r'.*\.jpg|.*\.png|.*\.mp4|.*\.jpeg')
                matched=match.search(str(file))
                if matched:
                      if matched.group().split(".")[-1]=="jpg" or matched.group().split(".")[-1]=="png" or matched.group().split(".")[-1]=="jpeg":
                                images_list.append({"filename":file,"dirpath":dirpath})
                      elif matched.group().split(".")[-1]=="mp4":
                                single_video_inference(dirpath+"/"+file)
      multiple_image_inference_benchmark(images_list,batch_size,model,gpu_before_inference,sheet,row_number)

def bechmark():
        row_number=0
        gpu_before_inference=get_gpu_consumption()
        tim_1=datetime.datetime.now()
        model = torch.hub.load("ultralytics/yolov5", Model_name)
        if Path(os.getcwd()+"/excel_gpu_consumption.xlsx").exists():
            excel=openpyxl.load_workbook("excel_gpu_consumption.xlsx")
        else:
            excel=openpyxl.Workbook()

        sheet=excel.active
        sheet.title="GPU_CONSUMED"
        sheet['A1']="Model-size"
        sheet['B1']="Batch-size"
        sheet['C1']="no of images"
        sheet['D1']="GPU_CONSUMED"
        sheet['E1']="tim_taken"
        while True:
            row_number+=1
            if sheet['A'+str(row_number)].value is None:
                 break
            else:
                 continue
        row_number=str(row_number)
        sheet['A'+row_number]=Model_name
        sheet['B'+row_number]=Batch_size
        sheet['C'+row_number]=no_of_files(file_name)
        inference_main_based_on_file_type(file_name,Batch_size,model,gpu_before_inference,sheet,row_number)
        tim_2=datetime.datetime.now()
        print("time taken",tim_2-tim_1)
        sheet['E'+row_number]=tim_2-tim_1
        excel.save("excel_gpu_consumption.xlsx")

bechmark()