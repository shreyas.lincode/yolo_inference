from gcp_bucket_class import GCPHelper
from mongo_helper import MongoHelper
import os
from zip import Zipfiles
import sys
from concurrent.futures import ThreadPoolExecutor
import numpy as np
from constants import *
# part_id="6242056667842b5b0bffbc1c"
# experiment_name="i100009"

# bucket_name_test="test_28001"
# blobname="prajodh_inference_test/"
# bucket_name="livis_datadrive"


class Dataloader:
    '''
    Dataloader class to download the zipfile images preprocesses them and get the ready for inferencing
    '''

    def __init__(self,part_id,experiment_id):
        self.zip_file_bucketname="livis_datadrive"
        self.zip_file_blobname=str(part_id)
        self.best_pt_blobname=str(experiment_id)
        self.experiment_id=experiment_id
        self.part_id=part_id

    def download_file_best_pt(self):
        '''
        Method to download the best.pt file for the given part and experiment id
        '''
        gcp_mover=GCPHelper()
        gcp_mover.set_bucket_name(self.zip_file_bucketname)
        gcp_mover.single_file_download(self.best_pt_blobname+"/runs/train/exp/weights/best.pt",os.getcwd())
        return os.getcwd()+"/best.pt"
    
    def download_zipfiles(self,filename):
        '''
        Method to download the zipfile with the images for the part from GCP
        '''
        zipfile_name=filename
        gcp_mover=GCPHelper()
        gcp_mover.set_bucket_name(self.zip_file_bucketname)
        gcp_mover.single_file_download(self.zip_file_blobname+"/"+zipfile_name,os.getcwd())
        self.file_path=os.getcwd()+"/"+zipfile_name
        return zipfile_name
    

    def upload_original_images(self,filename,inference_id):
        i=0
        batch_upload_list=[]
        original_image_document={}
        for dirpath,dirname,filenames in os.walk(filename):
            for file in filenames:
                original_image_document[str(i)]="https://storage.googleapis.com/livis_datadrive/"+self.zip_file_blobname+"/original_images/"+file
                path=dirpath+"/"+file
                batch_upload_list.append((path,file))
                print(dirpath+file,)
                i+=1
        
        index_num=[]
        counter_variable=1
        while counter_variable*4 < len(batch_upload_list):
            index_num.append(counter_variable*4)
            counter_variable+=1
        new_batch_list=np.split(batch_upload_list,index_num)

        gcp_mover=GCPHelper()
        gcp_mover.set_bucket_name(self.zip_file_bucketname)

        for batch in new_batch_list:
            print(i)
            with ThreadPoolExecutor(max_workers=4) as excecutor:
                if len(batch)>=1:
                    excecutor.submit(gcp_mover.single_file_transfer,batch[0][0],self.zip_file_blobname+"/original_images/"+batch[0][1])
                if len(batch)>=2:
                    excecutor.submit(gcp_mover.single_file_transfer,batch[1][0],self.zip_file_blobname+"/original_images/"+batch[1][1])
                if len(batch)>=3:
                    excecutor.submit(gcp_mover.single_file_transfer,batch[2][0],self.zip_file_blobname+"/original_images/"+batch[2][1])
                if len(batch)>=4:
                     excecutor.submit(gcp_mover.single_file_transfer,batch[3][0],self.zip_file_blobname+"/original_images/"+batch[3][1])

        
        db_object=MongoHelper()
        message, inference_manager, status_code = db_object.get_collection(INFERENCE_COLLECTION)
        if status_code==200:
             document=db_object.find(inference_id,single=True)
        else:
             print("couldnt connect to db")
             sys.exit(1)
        inference_manager.update_one({'_id': inference_id},{"$set":{"original_images":original_image_document}})



    def walk_through_files_task(self):
        '''
        function to extract the zipfiles, change non compatible image formats and validate the images,videos
        '''
        file_obj=Zipfiles(self.file_path)
        file_obj.extract_zip_files()
        file_obj.walk_through_correct()
        file_obj.walk_through_validity()
