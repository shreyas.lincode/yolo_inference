import pymongo
import tldextract
from pymongo import MongoClient
from threading import local
from bson.objectid import ObjectId
from constants import *

COMMON_COLLECTIONS = ["permissions", "camera_list", "domains"]

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@singleton
class MongoHelper:
    client = None

    def __init__(self):
        if not self.client:
            self.client = MongoClient(host=MONGO_HOST, port=MONGO_PORT)
        self.db = self.client[MONGO_DB]
        self.DB = None
        self.db_collection_name = None

    def getDatabase(self):
        return self.db

    def create_index(self,collection_name, field_name):
        self.get_collection(collection_name).create_index([(field_name, pymongo.TEXT)], name='search_index', default_language='english')

    def get_collection(self, collection_name):
        """
        This method returns the collection from MongoDB.
        If collection is not found, a new collection is created and returned.

        collection_name: (str) -> collection name
        domain_override: (str) -> Prefix for collection. 

        return: collection object if success else returns None
        """

        if not collection_name:
            return "Bad Request: Collection name is None", None, 400
        self.db_collection_name = self.db[collection_name]
        if self.db_collection_name is not None:
            return  "SUCCESS", self.db_collection_name, 200
        else:
            return "FAILED",{},400

    
    def find(self, mongo_id, search_query={}, single=False, reverse_sort=False, skip=-1, limit=-1):
        if single:
            docs = self.get_single(mongo_id)
        else:
            docs = self.get_multiple(search_query, reverse_sort, skip, limit)
        return docs


    def get_multiple(self, search_query, reverse_sort, skip, limit):
        if reverse_sort:
            if skip >= 0 and limit >= 0:
                docs = self.db_collection_name.find(search_query).sort("$natural", -1).skip(skip).limit(limit)
            else:
                docs = self.db_collection_name.find(search_query).sort("$natural", -1)
        elif skip >= 0 and limit >= 0:
            docs = self.db_collection_name.find(search_query).skip(skip).limit(limit)
        else:
            docs = self.db_collection_name.find(search_query)
        return docs


    def get_single(self, mongo_id):
        docs = self.db_collection_name.find_one({'_id': ObjectId(mongo_id)})
        return docs

    
    def update_single(self, document,change):
        self.db_collection_name.update_one({'_id': document['_id']}, {'$set': change})
        return "Success"

    
    def insert_single(self, document):
        _id  = self.db_collection_name.insert_one(document)
        print("inside common utils",_id)
        return _id.inserted_id


    def get_count(self, search_query= {}):
        count = self.db_collection_name.count_documents(search_query)
        return count