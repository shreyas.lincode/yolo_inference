from google.cloud import storage
import os
import traceback
from abc import ABC, abstractmethod

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


class DataMover(ABC):  ## can be used for moving data

    def __init__(self):
        self.client = None

    @abstractmethod
    def single_file_transfer(self):
        return


@singleton
class GCPHelper(DataMover):
    def __init__(self):
        super().__init__()
        if not self.client:
            os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'first-campaign-346416-e263efdbed96.json'
            self.client = storage.Client()
        self.bucket_name = ""

    def set_bucket_name(self, name):
        self.bucket_name = name

    def list_blobs(self):
        bucket_con = []
        """Lists all the blobs in the bucket."""
        # Note: Client.list_blobs requires at least package version 1.17.0.
        blobs = self.client.list_blobs(self.bucket_name)
        for blob in blobs:
            # print(blob.name)
            bucket_con.append(blob.name)
        return bucket_con

    def upload_to_bucket(self, blob_name, file_path):
        '''
        Upload file to a bucket
        : blob_name  (str) - object name
        : file_path (str)
        : bucket_name (str)
        '''
        bucket = self.client.get_bucket(self.bucket_name)
        blob = bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return blob.public_url

    def single_file_transfer(self, input_file_name, output_file_name):
        print("Copying File name:", input_file_name)
        self.upload_to_bucket(output_file_name, input_file_name)

    
    def single_file_download(self,download_file_name,save_folder_name):
        '''
        download file from bucket
        download_file_name
        save_folder_name
        '''
        print("Downloading File name:",download_file_name)
        bucket =self.client.get_bucket(self.bucket_name)
        blob=bucket.blob(download_file_name)
        name=str(blob.name).split("/")[-1]
        destination_uri = '{}/{}'.format(save_folder_name,name)
        blob.download_to_filename(destination_uri)


 