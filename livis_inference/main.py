from inference import Inference
from data_loader import Dataloader
from mongo_helper import MongoHelper
import sys
import threading
import os
from constants import *

"""
Move constants to Inference class and use all caps for constants. -done
Add multithreading to download the weights and download the zip file and for uploading back to GCP. -to do after gcp and mongo creation
Move this to a seperate Module-Done in inferencing.py file
rename Inference as Dataloader - created a dataloader class wich is seperate
replace place holders-
Don't rename the input file names- Done
Rename predicted files as <filename>_result.<format> -Done
"""


def get_inferencing_details():
    '''
    fuction to get all the inferecing input parameters from
    the mongo document
    '''
    db_obj=MongoHelper()
    message, inference_manager, status_code = db_obj.get_collection(INFERENCE_COLLECTION)
    if status_code == 200:
        document = inference_manager.find_one({"status": "picked","isRunning":False})
        if document==None:
            return (None, None, None, None)
        db_obj.update_single(document,{"status":"inferencing","isRunning":True})
        return document["_id"], document['part_id'], document['experiment_id'], document['source_url']
    else: 
        return (None, None, None, None)



def complete_inference_mongo_update(inference_id,download_file_url):
     '''

     fuction to update the mongodocument and the statuses while inferencing

     '''
     db_obj=MongoHelper()
     message, inference_manager, status_code = db_obj.get_collection(INFERENCE_COLLECTION)
     if status_code == 200:
        document=db_obj.find(inference_id,single=True)
        db_obj.update_single(document,{"status":"completed","results_available":True,"isRunning":False,"download_url":download_file_url})
     else:
        sys.exit("could not connect to db")



def run_inference():
    '''
    main inferencing function to start all required processes
    
    '''
    inference_id, part_id, experiment_id, source_url=get_inferencing_details()

    if inference_id is None:
        print(f"\n\nNo inference jobs available\n\n")
        sys.exit(1)

    dataloader_obj=Dataloader(part_id,experiment_id)
    t1=threading.Thread(target=dataloader_obj.download_file_best_pt)
    t1.start()
    filename=dataloader_obj.download_zipfiles(str(source_url).split("/")[-1])
    t1.join()
    dataloader_obj.walk_through_files_task()
    dataloader_obj.upload_original_images(os.getcwd()+"/"+filename.split(".zip")[0]+"_good_images",inference_id)

    inference_obj=Inference(filename.split(".zip")[0]+"_good_images",4,part_id)
    inference_obj.inference_main_based_on_file_type()
    download_file_url=inference_obj.upload_all_inference_files_to_gcp(inference_id)
    complete_inference_mongo_update(inference_id,download_file_url)



if __name__ == "__main__":
    run_inference()
    print(f"\n\n\nInference job completed...\n\n\n")