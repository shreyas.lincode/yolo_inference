import zipfile
import os
from pathlib import Path
import glob
import cv2
import re
import shutil
from PIL import Image
from PIL import Image
import pillow_heif
from datetime import datetime
import magic



def rename_file(url):
    '''
    input:file input is url
    filename format is : inference_[datime.now()].zip
    output:new file url,new file name
    '''
    file_name="inference"+str(datetime.now())+".zip"
    os.rename(url,url.split("/"+url.split('/')[-1].split('.zip')[0]+".zip")[0]+"/"+file_name)
    new_url=url.split("/"+url.split('/')[-1].split('.zip')[0]+".zip")[0]+"/"+file_name
    return new_url,file_name

class Zipfiles:
    def __init__(self,url):
        self.url=url
        self.folder=url.split('/')[-1].split('.zip')[0]
        self.file_url=url.split("/"+self.folder+".zip")[0]+"/"
        print(self.url)
        print(self.folder)
        print(self.file_url)
        
        


    def inspect_zip_file(self):
        '''
        function to inspect and walk through the zipfile
        '''
        with zipfile.ZipFile(self.url,"r") as zip:
         print(zip.printdir())


    def extract_zip_files(self):
        '''
        function to extract contents of zipfile
        '''
        os.chdir(self.file_url)
        try:
            files=zipfile.ZipFile(self.url,"r")
            files.extractall(self.file_url+self.folder)
            print("done extracting")
            files.close()
        except zipfile.BadZipFile:
            print("file is corupt")
            return



    def walk_through_correct(self):
         '''
         function to walk through files and rename non compatible image extension types
         '''
         for dirpath,dirnames,filenames in os.walk(self.file_url+self.folder):
            for i in filenames:
              path=dirpath+"/"+i
              match=re.compile(r'.*\.heic|.*\.bmp')
              matched = match.search(str(i))
              if matched:
                    if matched.group().split(".")[-1]=="heic":
                        heif_file = pillow_heif.read_heif(path)
                        os.remove(path)
                        image = Image.frombytes(
                        heif_file.mode,
                        heif_file.size,
                        heif_file.data,
                        "raw" )
                        base = os.path.splitext(str(i))[0]
                        os.chdir(dirpath)
                        image.save(base+'.png', format("png"))
                        os.chdir(dirpath+'/..')
                    elif matched.group().split(".")[-1]=="bmp":
                          base = os.path.splitext(str(i))[0]
                          img = Image.open(path)
                          os.remove(path)
                          os.chdir(dirpath)
                          img.save(base+'.png','png')
                          os.chdir(dirpath+'/..')
         print("done converting")



    def walk_through_validity(self):
        '''
        function to walk through files and check image and video validity
        '''
        os.chdir(self.file_url)
        os.mkdir(self.file_url+self.folder+"_bad_images")
        os.mkdir(self.file_url+self.folder+"_good_images")
        for dirpath,dirnames,filenames in os.walk(self.file_url+self.folder):
            for i in filenames:
                  path=dirpath+"/"+i
                  try:
                     match=re.compile(r".*\.mp4|.*\.mov")
                     matched=match.search(str(i))
                     if matched:
                         if magic.from_buffer(open(path, "rb").read(2048))=="empty":
                                print(f"The file is File path {path} IS INVALID")
                                shutil.move(path,self.file_url+self.folder+"_bad_images/"+i)
                                print("moved the file away to bad_files folder")
                         else:
                                print(f"The file is Video file path {path} IS VALID")
                                shutil.move(path,self.file_url+self.folder+"_good_images/"+i)
                                print("moved the file away to good_images folder")     
                         continue   
                     img=cv2.imread(path)
                     height,width,color_channels=img.shape
                     if height or width or color_channels !=0:
                        print(f"The file is File path {path} IS VALID")
                        shutil.move(path,self.file_url+self.folder+"_good_images/"+i)
                        print("moved the file away to good_images folder")
                     else:
                         print(f"The file is File path {path} IS INVALID")
                         shutil.move(path,self.file_url+self.folder+"_bad_images/"+i)
                         print("moved the file away to bad_files folder")
                  except:
                        print(f"The file is File path {path} IS INVALID")
                        shutil.move(path,self.file_url+self.folder+"_bad_images/"+i)
                        print("moved the file away to bad_files folder")