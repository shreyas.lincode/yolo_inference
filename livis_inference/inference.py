import torch
import os
import numpy as np
from gcp_bucket_class import GCPHelper
import os
import re
import shutil
import detect
from concurrent.futures import ThreadPoolExecutor
import sys
from mongo_helper import MongoHelper
from constants import *

class Inference:
    
    def __init__(self,filename,batch_size,part_id):
            self.bucket_name="livis_datadrive"
            self.blobname=part_id
            self.filename=filename
            self.batch_size=batch_size
            self.path_best_pt="best.pt"

    def multiple_image_inference(self,image_list):
        '''
        does batch inferencing of the images
        '''
        model=torch.hub.load('ultralytics/yolov5', 'custom', path=os.getcwd()+"/"+'best.pt', force_reload=True)
        batch_size=self.batch_size
        counter_variable=1
        images=image_list
        batch_size_list=[]

        while counter_variable*batch_size < len(images):
            batch_size_list.append(counter_variable*batch_size)
            counter_variable+=1

        images=np.split(images,batch_size_list)
        for img in images:
            imgs=[file['dirpath']+"/"+file['filename'] for file in img]
            results=model(imgs)
            results.save()

    def single_video_inference(self,video_file):
        '''
        does video inferencing
        '''
        path_best_pt="best.pt"
        print(os.getcwd())
        detect.run(weights=path_best_pt,source=video_file,project=os.getcwd()+"livis_inference/runs/detect/")



    def inference_main_based_on_file_type(self):
        '''
        input : unzipped folder name
        functionality: to do the appropriate inferening based on file type(images and videos)
        output:None
        '''
        images_list=[]
        for dirpath,dirname,filenames in os.walk(os.getcwd()+"/"+self.filename):
                for file in filenames:  
                    match=re.compile(r'.*\.jpg|.*\.png|.*\.mp4|.*\.jpeg|.*\.mov')
                    matched=match.search(str(file))
                    if matched:
                        if matched.group().split(".")[-1]=="jpg" or matched.group().split(".")[-1]=="png" or matched.group().split(".")[-1]=="jpeg":
                                    images_list.append({"filename":file,"dirpath":dirpath})
                        elif matched.group().split(".")[-1]=="mp4" or matched.group().split(".")[-1]=="mov":
                                    self.single_video_inference(dirpath+"/"+file)
        self.multiple_image_inference(images_list)



    def upload_all_inference_files_to_gcp(self,inference_id):
        '''
        uploads all the inferenced images, videos and zipfiles of the inferenced and bad images to
        GCP
        '''
        gcp_mover=GCPHelper()
        gcp_mover.set_bucket_name(self.bucket_name)
        batch_upload_list=[]
        for dirpath,dirnames,filenames in os.walk(os.getcwd()+"/runs/"):
                for i in filenames:
                    path=dirpath+"/"+i
                    filename=i
                    print(filename)
                    batch_upload_list.append((path,filename))
        print(batch_upload_list)
        index_num=[]
        counter_variable=1
        while counter_variable*4 < len(batch_upload_list):
            index_num.append(counter_variable*4)
            counter_variable+=1
        new_batch_list=np.split(batch_upload_list,index_num)

        for batch in new_batch_list:
            with ThreadPoolExecutor(max_workers=4) as excecutor:
                if len(batch)>=1:
                    excecutor.submit(gcp_mover.single_file_transfer,batch[0][0],self.blobname+"/infered_images_videos/"+batch[0][1].split(".")[0]+"_result"+"."+batch[0][1].split(".")[-1])
                if len(batch)>=2:
                    excecutor.submit(gcp_mover.single_file_transfer,batch[1][0],self.blobname+"/infered_images_videos/"+batch[1][1].split(".")[0]+"_result"+"."+batch[1][1].split(".")[-1])
                if len(batch)>=3:
                    excecutor.submit(gcp_mover.single_file_transfer,batch[2][0],self.blobname+"/infered_images_videos/"+batch[2][1].split(".")[0]+"_result"+"."+batch[2][1].split(".")[-1])
                if len(batch)>=4:
                     excecutor.submit(gcp_mover.single_file_transfer,batch[3][0],self.blobname+"/infered_images_videos/"+batch[3][1].split(".")[0]+"_result"+"."+batch[3][1].split(".")[-1])
        
        
        image_obj={}
        num=0
        file_=self.filename.split("_")[0]+"_"+self.filename.split("_")[1]+"_download_results"
        print(file_)
        os.mkdir(file_)
        for dirpath,dirname,filenames in os.walk(os.getcwd()+"/runs/"):
             for file in filenames:
                  file_name=file.split(".")[0]+"_result"+"."+file.split(".")[-1]
                  image_obj[str(num)]="https://storage.googleapis.com/livis_datadrive/"+self.blobname+"/infered_images_videos/"+file_name
                  num+=1
                  shutil.move(dirpath+"/"+file,os.getcwd()+"/"+file_)
        shutil.make_archive(file_,"zip",os.getcwd()+"/"+file_)
        gcp_mover.single_file_transfer(os.getcwd()+"/"+file_+".zip",self.blobname+"/"+file_+".zip")
        download_file_url="https://storage.googleapis.com/livis_datadrive/"+self.blobname+"/"+file_+".zip"


        db_object=MongoHelper()
        message, inference_manager, status_code = db_object.get_collection(INFERENCE_COLLECTION)
        if status_code==200:
             document=db_object.find(inference_id,single=True)
        else:
             print("couldnt connect to db")
             sys.exit(1)
        inference_manager.update_one({'_id': inference_id},{"$set":{"infered_image":image_obj}})
        
        bad_images_folder=self.filename.split("_")[0]+"_"+self.filename.split("_")[1]+"_bad_images"
        if os.listdir(os.getcwd()+"/"+bad_images_folder):
            shutil.make_archive(bad_images_folder,"zip",bad_images_folder)
            gcp_mover.single_file_transfer(os.getcwd()+"/"+bad_images_folder+".zip",self.blobname+"/"+bad_images_folder+".zip")

        return download_file_url
        